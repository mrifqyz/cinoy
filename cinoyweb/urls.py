from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('story4.urls')),
    path('story1/', include('story1.urls')),
    path('story5/', include('story5.urls')), 
    path('story3/', include('story3.urls'))
]
