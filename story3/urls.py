from django.urls import path, include
from .views import story3, surprise

urlpatterns = [ 
    path('', story3),
    path('surprise', surprise)
]