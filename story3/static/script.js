$(document).ready(function(){
    $("#home").css("border-bottom", "2px solid #F76C6C");
    $("#home").click(function (){
        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 1000);

        $(".nav-item").css("border-bottom", "none");
        $("#home").css("border-bottom", "2px solid #F76C6C");
    });

    $("#goto-project").click(function (){
        $('html, body').animate({
            scrollTop: $("#project").offset().top
        }, 1000);

        $(".nav-item").css("border-bottom", "none");
        $("#goto-project").css("border-bottom", "2px solid #F76C6C");
    });

    $("#goto-about").click(function (){
        $('html, body').animate({
            scrollTop: $("#about").offset().top
        }, 1000);

        $(".nav-item").css("border-bottom", "none");
        $("#goto-about").css("border-bottom", "2px solid #F76C6C");
    });

    $("#goto-contact").click(function (){
        $('html, body').animate({
            scrollTop: $("footer").offset().top
        }, 1000);

        $(".nav-item").css("border-bottom", "none");
        $("#goto-contact").css("border-bottom", "2px solid #F76C6C");
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
            $('#scroll-here').css({
                'color': '#f2f2f2'
            });

            $('.vr').css({
                'border-left': 'none'
            });
            
        }

        if($(this).scrollTop() == 0){
            $('#scroll-here').css({
                'color': '#434343'
            });

            $('.vr').css({
                'border-left': '2px solid #434343'
            });
        }

    });
})