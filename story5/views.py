from django.shortcuts import render, HttpResponseRedirect
from .forms import JadwalForm
from .models import Jadwal

import datetime

# Create your views here.
def landing(request):
    return render(request, 'jadwal-main.html')

def inputpage(request):
    form = JadwalForm()

    if request.method == "POST":
        form = JadwalForm(request.POST)

        format_t = str(request.POST['tanggal_jam_form']).split(" ")
        format_jam = format_t[1] + ":00"
        day, month, year = map(int, format_t[0].split('/'))
        format_tanggal = str(year) + "-" + str(month) + "-" + str(day)

        if form.is_valid():
            Jadwal.objects.create(
                kategori = request.POST['kategori_form'],
                kegiatan = request.POST['kegiatan_form'],
                tempat = request.POST['tempat_form'],
                hari = datetime.date(year, month, day).strftime('%A'),
                tanggal = format_tanggal,
                jam = format_jam
            )

            return HttpResponseRedirect('/story5/scheldue')

    context = {
        'form':form
    }
    return render(request, "inputpage.html",context)

def scheldue(request):
    data_jadwal = Jadwal.objects.all()
    context = {
        'CONTENT': data_jadwal
    }
    return render(request, 'jadwal.html', context)

def delete(request, id_jadwal):
    Jadwal.objects.filter(id=id_jadwal).delete()
    return HttpResponseRedirect('/story5/scheldue') 

    