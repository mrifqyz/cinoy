from django.db import models
import datetime

# Create your models here.
class Jadwal(models.Model):
    kategori = models.CharField(max_length = 50)
    kegiatan = models.CharField(max_length = 50)
    tempat = models.CharField(max_length = 50)
    hari = models.CharField(max_length=20)
    tanggal = models.DateField()
    jam = models.TimeField()

    def __str__(self):
        return self.kegiatan

    def date_day(self):
        new_tanggal = str(self.tanggal)
        year, month, day = map(int,new_tanggal.split('-'))

        new_month = datetime.date(year, month, day).strftime("%B")

        if(day == 1):
            new_day = "1st"
        elif(day == 2):
            new_day = "2nd"
        elif(day==3):
            new_day = "3rd"
        else:
            new_day = str(day) + "th"

        return self.hari + ', ' + str(new_month) + " " + str(new_day) + " " + str(year) 

    def time(self):
        jam_temp = str(self.jam)
        jam_asli = jam_temp.split(":")
        return jam_asli[0] + "." + jam_asli[1]