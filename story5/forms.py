from django import forms

from .models import Jadwal

class JadwalForm(forms.Form):
    kategori_form = forms.CharField(label='Category',
     widget= forms.TextInput(attrs={
        'class': 'form-control',
            'id': 'kategori',
        }))

    kegiatan_form = forms.CharField(label='Activities',
    widget= forms.TextInput(attrs={
        'class': 'form-control',
            'id': 'kegiatan',
        }))

    tempat_form = forms.CharField(label='Location', required=False,
    widget= forms.TextInput(attrs={
        'class': 'form-control',
            'id': 'tempat',
        }))
    
    tanggal_jam_form = forms.DateTimeField(label='Date & Time',
    input_formats=['%d/%m/%Y %H:%M'],
    widget= forms.DateTimeInput(attrs={
            'id': 'dateTime',
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker1'
        })
        )
    
