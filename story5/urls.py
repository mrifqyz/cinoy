from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('', landing),
    re_path(r'^delete/(?P<id_jadwal>[0-9])/$', delete, name='delete'),
    path('create', inputpage),
    path('scheldue', scheldue)
]